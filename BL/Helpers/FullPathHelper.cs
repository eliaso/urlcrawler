﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elias.BL.Helpers
{
    public static class FullPathHelper
    {
        public static string BuildPath(this string url, string refererUrl)
        {
            var result = string.Empty;
            if (!string.IsNullOrWhiteSpace(url) && !string.IsNullOrWhiteSpace(refererUrl))
            {
                var uri = new Uri(refererUrl);
                var port = uri.Port == 80 ? "" : ":" + uri.Port;
                if (url.StartsWith("/") && !url.StartsWith("//"))
                    result = "http://" + uri.Host + port + url ;
                else if (!url.StartsWith("/") && !url.StartsWith("http"))
                {
                    var relativePath = string.Join("", uri.Segments.Take(uri.Segments.Length - 1));
                    if (string.IsNullOrEmpty(relativePath))
                        relativePath = "/";
                    result = "http://" + uri.Host + port + relativePath + url;
                }
                else
                    result = url;
            }
            return result;
        }
    }
}
