﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Elias.Models;
using HtmlAgilityPack;

namespace Elias.BL
{
    public class ParsePageService
    {

        public ParsedPage ParsePage(string url)
        {
            ParsedPage result = new ParsedPage() {SourceUrl = url};
            //HttpClient client = new HttpClient();
            //string rawHtml = client.GetStringAsync(url).Result;

            var client = new WebClient();
            

            try
            {
                string rawHtml = client.DownloadString(url);
                result = GetParsedPage(url, rawHtml);
            }
            catch (Exception ex)
            {
                result.Status = string.Format("Error loading {0} - {1}", url, ex.Message);
            }
            return result;
        }

        public List<WordStat> GetWordStats(string htmlText)
        {
            char[] delimiterChars = { ' ', ',', '.', ':', '?', '!','\r' };
            var excludedWords = new List<string> {"a", "the", "an"};
            var words = htmlText.ToLower().Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);
            var result = words
                .Where(w=> excludedWords.All(excluded => excluded != w))
                .GroupBy(w => w)
                .Select(x => new WordStat(x.Key, x.Count()))
                .OrderByDescending(x=> x.Count)
                .ThenByDescending(x=> x.Word.Length)
                .Take(10)
                .ToList();
            return result;

        }

        private string RemoveMarkupFromHtml(HtmlDocument htmlDoc)
        {
            var result = htmlDoc.DocumentNode.Descendants()
                .Where(dn =>
                    dn.Name == "#text"
                    && dn.ParentNode.Name != "title"
                    && dn.ParentNode.Name != "script"
                    && dn.ParentNode.Name != "style"
                    && !string.IsNullOrWhiteSpace(dn.InnerText)
                )
                .Select(dn => new
                {
                    dn.InnerText
                })
                .Aggregate("", (resultSoFar, dn) => resultSoFar + " " + dn.InnerText.Replace("\n", ""));
            return result;
        }

        public ParsedPage GetParsedPage(string url, string rawHtml)
        {
            var result = new ParsedPage() { SourceUrl = url };
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(rawHtml);
            var imageUrls = GetImageSources(htmlDoc.DocumentNode.SelectNodes("//img[@src]"), url);
            result.ImageUrls = imageUrls;

            var words = RemoveMarkupFromHtml(htmlDoc);
            result.WordStats = GetWordStats(words);
            return result;
        }

        private List<string> GetImageSources(HtmlNodeCollection imageElements, string url)
        {
            if (imageElements == null)
                return new List<string>();
            var imageSources = imageElements.Select(x => x.Attributes["src"].Value).ToList();
            return imageSources;
        }
    }
}
