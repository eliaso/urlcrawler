﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;

namespace Elias.Models
{
    public class ParsedPage
    {
        public string SourceUrl { get; set; }
        public List<string> ImageUrls { get; set; }
        public List<WordStat> WordStats { get; set; }
        public string Status { get; set; }

        public ParsedPage()
        {
            ImageUrls = new List<string>();
        }
    }

    public class WordStat
    {
        public string Word { get; set; }
        public int Count { get; set; }
        
        public WordStat(string word, int count)
        {
            Word = word;
            Count = count;
        }
    }
}
