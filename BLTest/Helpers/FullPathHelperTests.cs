﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Elias.BL.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elias.BL.Helpers;
namespace Elias.BL.Helpers.Tests
{
    [TestClass()]
    public class FullPathHelperTests
    {
        [TestMethod()]
        public void BuildPathToPageRelativeUrlinSubdirectory()
        {
            var referer = "http://example.com/sample/test.html";
            var expected = "http://example.com/sample/image.jpg";
            BuildPathTest(referer, "image.jpg", expected);
        }

        [TestMethod()]
        public void BuildPathToPageRelativeUrlinRoot()
        {
            var referer = "http://example.com/test.html";
            var expected = "http://example.com/image.jpg";
            BuildPathTest(referer, "image.jpg", expected);

            BuildPathTest("http://example.com/", "image.jpg", expected);
            BuildPathTest("http://example.com", "image.jpg", expected);
        }


        [TestMethod()]
        public void BuildPathToSiteRelativeUrlinSubdirectory()
        {
            var referer = "http://example.com/sample/test.html";
            var expected = "http://example.com/image.jpg";
            BuildPathTest(referer, "/image.jpg", expected);
        }

        private void BuildPathTest(string refererUrl, string imageurl, string expect)
        {
            var actual = imageurl.BuildPath(refererUrl);
            Assert.AreEqual(expect, actual, " when referer is {0}", refererUrl);
        }
    }
}