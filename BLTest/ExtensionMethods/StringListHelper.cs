﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Elias.BLTest.ExtensionMethods
{
    public static class StringListHelper
    {
        public static void Has(this List<string> list, string expected, string context)
        {
            Assert.IsTrue(list.Any(x=> x==expected), "{0} should contain {1}", context, expected);
        }
    }
}
