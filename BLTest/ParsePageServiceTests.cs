﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Elias.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Elias.BLTest.ExtensionMethods;
using Elias.Models;

namespace Elias.BL.Tests
{
    [TestClass()]
    public class ParsePageServiceTests
    {

        [TestMethod()]
        public void GetWordStatsTest()
        {
            var rawHtml = GetSampleContent();
            
            var service = new ParsePageService();
            var parsedPage = service.GetParsedPage("", rawHtml);
            var results = parsedPage.WordStats;

            Console.WriteLine("Word, Count:");
            results.ForEach(w => { Console.WriteLine("{0}, {1}", w.Word, w.Count);});

            var expected = new List<WordStat>
            {
                new WordStat("hello",1),
                new WordStat("big",2),
                new WordStat("great",1),
                new WordStat("world",3),
            };

            Assert.AreEqual(expected.Count, results.Count, "Number of words counted should be as expected");

            expected.ForEach(exp =>
            {
                var actual = results.Where(a => a.Word == exp.Word).Select(a => a.Count).FirstOrDefault();
                Assert.AreEqual( exp.Count, actual, "Count of {0} should be {1} but was {2}", 
                    exp.Word, exp.Count, actual);

            });


        }
        [TestMethod()]
        public void GetParsedPageTest()
        {
            var svc = new ParsePageService();
            var r = svc.GetParsedPage("http://example.com/go", GetSampleContent());

            //verify image parsing works right
            Assert.IsNotNull(r.ImageUrls, "Image urls should not be null");
            Assert.IsTrue(r.ImageUrls.Count == 4, "Expecting 4 image urls but found {0}", r.ImageUrls.Count);

            Console.WriteLine("ImageUrls:");
            r.ImageUrls.ForEach(i => { Console.WriteLine(i);});

            r.ImageUrls.Has("/sample1.jpg", "Images");
            r.ImageUrls.Has("sample2.jpg", "Images");
            r.ImageUrls.Has("http://example.com/absolusepathImage.jpg", "Images");
            r.ImageUrls.Has("//example.com/absolusepathImage_WithNoProtocolspecified.jpg", "Images");

            //r.ImageUrls.Has("http://example.com/sample1.jpg", "Images");
            //r.ImageUrls.Has("http://example.com/go/sample2.jpg", "Images");
            //r.ImageUrls.Has("http://example.com/absolusepathImage.jpg", "Images");
            //r.ImageUrls.Has("//example.com/absolusepathImage_WithNoProtocolspecified.jpg", "Images");

            Assert.AreEqual(r.SourceUrl, "http://example.com/go", "Source url should be the url i requested");
        }

        private void ImageUrlShouldExist(List<string> imagesUrls, string expectedUrl)
        {
            Assert.IsTrue(imagesUrls.Any(i => i == expectedUrl), "one of the images should be {0}", expectedUrl);
        }


        private string GetSampleContent()
        {
            return @"<html><head><title>Example Title</title></head><body>
<style>
hello{}
.world.style{}
</style>
<h1>Hello world</h1>
<img src=""/sample1.jpg"" />
<img src=""sample2.jpg"" />
<img src=""http://example.com/absolusepathImage.jpg"" />
<img src=""//example.com/absolusepathImage_WithNoProtocolspecified.jpg"" />
Big great big world. A World! 
<script>var Script_text_here = ""javascript-content"";</script>
</body></html>";
        }
    }
}