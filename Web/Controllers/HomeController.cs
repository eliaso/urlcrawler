﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Elias.BL;
using Elias.Models;
using Microsoft.Owin;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "HtmlWiz";
            ParsedPage model;
            var url = Request.QueryString["url"];
            if (!string.IsNullOrWhiteSpace(url))
                model = new ParsePageService().ParsePage(url);
            else
                model = new ParsedPage();
            
            return View(model);
        }
    }
}
