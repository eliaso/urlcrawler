﻿using System.Collections.Generic;
using System.Web.Http;
using Elias.Models;

namespace Web.Controllers
{
    public class ParseController: ApiController
    {
        // GET api/<controller>
        public string Get()
        {
            return "please provide a target url, replace :// with || and . with |. - ie http||example|com = http://example.com";
        }

        // GET api/Parse/5
        public ParsedPage Get(string id)
        {
            //System.Diagnostics.Debug.Assert(!string.IsNullOrWhiteSpace(id), " ");
            var url = id.Replace("||", "://").Replace("|", ".");
            var result = new Elias.BL.ParsePageService().ParsePage(url);
            return result;
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/Parse/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/Parse/5
        public void Delete(int id)
        {
        }
    }
}